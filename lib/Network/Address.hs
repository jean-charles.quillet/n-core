{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Network.Address (module NS             ,
                        Endpoint(..)          ,
                        Error(..)             ,
                        Interface(..)         ,
                        PortRange(..)         ,
                        Protocol(..)          ,
                        Scoped(..)            ,
                        VRF(..)               ,
                        VRFId(..)             ,
                        address               ,
                        addressBits           ,
                        addressFamily         ,
                        addressFromSockAddr   ,
                        addressLengthFor      ,
                        family                ,
                        globalVRFId           ,
                        inspectTLS            ,
                        interfaces            ,
                        ips                   ,
                        isIP4Private          ,
                        isIP4Reserved         ,
                        isIP6Private          ,
                        isIP6Reserved         ,
                        isIPReserved          ,
                        isV4                  ,
                        isV6                  ,
                        name                  ,
                        node                  ,
                        peekIP                ,
                        pokeIP                ,
                        port                  ,
                        portNumber            ,
                        portFromSockAddr      ,
                        protocolNumber        ,
                        sockAddrFrom          ,
                        toIPv6                ,
                        validHost             ,
                        v4NetworkAddress      ,
                        v6NetworkAddress      ,
                        vrfId                 ,
                        vrfName               ) where

import Codec.Serialise             (Serialise)
import Control.Applicative         ((<|>))
import Control.Exception           (Exception, SomeException, catch, throw)
import Control.Lens         hiding ((.=))
import Control.Monad               (guard, void)
import Control.Monad.State         (State, execState, modify)
import Data.Aeson     as JSON
import Data.Aeson.Types            (unexpected)
import Data.Hashes.FNV1a           (fnv1aHash32Word32, fnvOffsetBasis32)
import Data.Hashable               (Hashable, hashWithSalt, hash)
import Data.Binary                 (Binary, get, put)
import Data.Bits                   ((.&.))
import Data.ByteString.Char8       (ByteString)
import Data.Char                   (isDigit, isHexDigit, isSpace, isNumber)
import Data.IP        as NS hiding (toIPv6)    
import Data.List                   (stripPrefix)
import Data.Maybe                  (fromMaybe)
import Data.Scientific             (toBoundedInteger)
import Data.Set                    (Set)
import Data.Text                   (pack, unpack)
import Data.Typeable               (Typeable)
import Data.Word                   (Word8, Word16, Word32)
import Foreign.Storable            (peek, poke)
import Foreign.Ptr                 (Ptr, plusPtr)
import Foreign.C.Types             (CInt)
import GHC.Generics                (Generic)
import Network.Socket as NS hiding (Raw)
import System.IO.Unsafe            (unsafePerformIO)
import Text.Read                   (readMaybe)


import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Char8            as B
import qualified Data.Text                        as T
import qualified Codec.Serialise                  as CBOR

data Endpoint  = Endpoint {
                    _address :: IP,
                    _port    :: PortNumber
                 } deriving (Eq, Ord)

makeFieldsNoPrefix 'Endpoint

instance Show Endpoint where 
    showsPrec _ endpoint = ((showAddr ++ ':':portNum) ++)
        where showAddr = case endpoint ^. address of 
                            IPv4 addr -> show addr
                            IPv6 addr -> '[':show addr ++ "]"
              portNum  = show $ endpoint ^. port 

instance Read Endpoint where
    -- | parse a V6 address
    readsPrec _ ('[':str)  =
        let (address', rest) = span (/= ']') $ dropWhile isSpace str -- extract the IPv6 portion
            rest'           = drop 2 rest       -- skip the ]:
            (port', remains) = span isDigit rest'
        in readEndpoint' address' port' remains
    -- | parse a V4 socket address
    readsPrec _ str =
        let (address', rest) = span (/= ':') $ dropWhile isSpace str -- extract IPv4 address
            (port', remains) = span isDigit $ drop 1 rest -- skip the : and expect the numeric port
        in readEndpoint' address' port' remains

readEndpoint' :: String -> String -> String -> [(Endpoint, String)]
readEndpoint' addrStr portStr remains =
    let addr  = read addrStr
        port' = read portStr
    in [(Endpoint addr port', remains)]

instance FromJSON Endpoint where
  parseJSON = withText "Endpoint" $ \txt ->
    maybe (unexpected $ JSON.String txt) return . readMaybe $ unpack txt

instance ToJSON Endpoint where
  toJSON = JSON.String . pack . show

data Error = BadSockAddr String
           | InvalidIPAddressLength Word8
           | UnknownProtocol Int
           | UnsupportedAddressFamily Family
             deriving (Show, Typeable) 

instance Exception Error 

data Protocol = Internet
              | Raw
              | PacketProto
              | GRE
              | UDP
              | TCP
              | ICMP
              | ICMPv6
              | ProtocolNumber Word8 deriving Eq

instance Bounded Protocol where
  minBound = ProtocolNumber 0
  maxBound = ICMPv6

instance Show Protocol where
    showsPrec _ GRE         = ("gre" ++)
    showsPrec _ ICMP        = ("icmp" ++)
    showsPrec _ ICMPv6      = ("icmpv6" ++)
    showsPrec _ Internet    = ("ip" ++)
    showsPrec _ PacketProto = ("packet" ++)
    showsPrec _ Raw         = ("raw" ++)
    showsPrec _ TCP         = ("tcp" ++)
    showsPrec _ UDP         = ("udp" ++)
    showsPrec p (ProtocolNumber 0)   = showsPrec p Internet
    showsPrec p (ProtocolNumber 6)   = showsPrec p TCP
    showsPrec p (ProtocolNumber 17)  = showsPrec p UDP
    showsPrec p (ProtocolNumber 47)  = showsPrec p GRE
    showsPrec p (ProtocolNumber 1)   = showsPrec p ICMP
    showsPrec p (ProtocolNumber 58)  = showsPrec p ICMPv6
    showsPrec _ (ProtocolNumber x)   = (show x ++)

instance Read Protocol where
  readsPrec _ str = read'
    where read'
            | Just rest <- stripPrefix "gre"    str' = [(GRE,         rest)]
            | Just rest <- stripPrefix "icmp"   str' = [(ICMP,        rest)]
            | Just rest <- stripPrefix "icmpv6" str' = [(ICMPv6,      rest)]
            | Just rest <- stripPrefix "ip"     str' = [(Internet,    rest)]
            | Just rest <- stripPrefix "packet" str' = [(PacketProto, rest)]
            | Just rest <- stripPrefix "raw"    str' = [(Raw,         rest)]
            | Just rest <- stripPrefix "tcp"    str' = [(TCP,         rest)]
            | Just rest <- stripPrefix "udp"    str' = [(UDP,         rest)]
            | otherwise = fromMaybe [] $ do
                            let (numStr, rest) = span isNumber str'
                            x <- readMaybe numStr
                            return [(ProtocolNumber x, rest)]
          str' = dropWhile isSpace str

instance Enum Protocol where
    toEnum 0     = Internet
    toEnum 1     = ICMP
    toEnum 6     = TCP
    toEnum 17    = UDP
    toEnum 47    = GRE
    toEnum 58    = ICMPv6
    toEnum 255   = Raw -- this is probably Linux specific. Sorry
    toEnum 2048  = PacketProto -- same here
    toEnum p     = fromMaybe (throw $ UnknownProtocol p) $ customProtocol p

    fromEnum Internet    = 0
    fromEnum GRE         = 47
    fromEnum UDP         = 17
    fromEnum TCP         = 6
    fromEnum ICMP        = 1
    fromEnum ICMPv6      = 58
    fromEnum Raw         = 255
    fromEnum PacketProto = 2048
    fromEnum (ProtocolNumber x) = fromIntegral x

customProtocol :: Int -> Maybe Protocol
customProtocol 0  = return Internet
customProtocol 1  = return ICMP
customProtocol 6  = return TCP
customProtocol 17 = return UDP
customProtocol 47 = return GRE
customProtocol 58 = return ICMPv6
customProtocol x  
  | x <= 255  = return $ ProtocolNumber (fromIntegral x)
  | otherwise = Nothing

protocolNumber :: Integral a => Protocol -> a  
protocolNumber = fromIntegral . fromEnum 

instance FromJSON Protocol where
  parseJSON = withText "Protocol" $ \txt ->
    maybe (unexpected $ JSON.String txt) return . readMaybe $ unpack txt

instance ToJSON Protocol where
  toJSON = JSON.String . pack . show

newtype VRFId = VRFId { unVRFId :: Int } deriving (Binary, Generic, Eq, Ord, Serialise, Num, Real, Enum, Integral)

instance Show VRFId where
  show (VRFId i) = show i

instance Read VRFId where
  readsPrec p = over (mapped . _1) VRFId . readsPrec p

instance FromJSON VRFId where
    parseJSON = withScientific "VRFId" $
                    fmap VRFId . maybe badValue return . toBoundedInteger
        where badValue = fail "VRF id is not a number"

instance ToJSON VRFId where
    toJSON (VRFId x) = Number $ fromIntegral x

-- naming convention exception below.. because of the lenses defined by makeFields, it must be
-- scopedVrfId.. and not scopedVRF as it should be.. otherwise the lens names come out like vRF ..
-- which is worse-er
data Scoped a = Scoped { scopedVrfId :: VRFId, scopedAddress :: a } deriving (Generic, Eq, Ord)

globalVRFId :: VRFId
globalVRFId = VRFId 0

instance Functor Scoped where
  fmap f (Scoped v a) = Scoped v (f a)

instance Binary a => Binary (Scoped a)

instance Read a => Read (Scoped a) where
    readsPrec _ str = fromMaybe [] $ do
        let (vrfStr, rest) = drop 1 <$> span (/= ':') (dropWhile isSpace str) -- extract the VRF
        vrfId'   <- readMaybe vrfStr
        address' <- readMaybe rest
        return [(Scoped vrfId' address', "")]

instance Show a => Show (Scoped a) where
    showsPrec _ (Scoped vrfId' address') =
        ((show vrfId' ++ ':':show address') ++)

instance Serialise a => Serialise (Scoped a)

makeFields ''Scoped

data Interface = Interface { _name :: String,
                             _ips  :: Set IP,
                             _node :: String }

  deriving (Read, Show, Generic, Eq, Ord)

makeFieldsNoPrefix ''Interface

instance FromJSON Interface where
    parseJSON = withObject "interface" $ \v ->
                    Interface <$> v .: "name"
                              <*> v .: "addresses"
                              <*> v .: "node"

instance ToJSON Interface where
    toJSON intf = object ["name"      .= T.pack (intf ^. name),
                          "addresses" .= (intf ^. ips)          ,
                          "node"      .= (intf ^. node)         ]
instance Serialise Interface
instance Binary Interface

data VRF = VRF { _vrfId      :: VRFId         ,
                 _vrfName    :: String        ,
                 _inspectTLS :: Bool          ,
                 _interfaces :: Set Interface }
            deriving (Read, Show, Generic, Eq, Ord)

makeFieldsNoPrefix ''VRF

instance Binary    VRF
instance Serialise VRF

instance FromJSON VRF where
    parseJSON = withObject "VRF" $ \v ->
                    VRF <$> v .: "id"
                        <*> v .: "name"
                        <*> v .: "inspect TLS"
                        <*> v .: "interfaces"

instance ToJSON VRF where
    toJSON vrf = object [ "id"          .= (vrf ^. vrfId)         ,
                          "name"        .= T.pack (vrf ^. vrfName),
                          "inspect TLS" .= (vrf ^. inspectTLS)    ,
                          "interfaces"  .= (vrf ^. interfaces)    ]

instance Binary IP
instance Binary IPv4
instance Binary IPv6
instance Binary (AddrRange IPv4)
instance Binary (AddrRange IPv6)

instance Serialise IPv4
instance Serialise IPv6
instance Serialise IP
instance Serialise (AddrRange IP)
instance Serialise (AddrRange IPv4)
instance Serialise (AddrRange IPv6)
instance Serialise IPRange

instance CBOR.Serialise PortNumber where
    encode = CBOR.encode . (fromIntegral :: PortNumber -> Word16)
    decode = (fromIntegral :: Word16 -> PortNumber) <$> CBOR.decode

instance FromJSON IPv4
instance FromJSON IPv6
instance FromJSON IP
instance FromJSON a => FromJSON (AddrRange a)

instance FromJSON IPRange where
    parseJSON = withText "IPRange" $ \txt ->
                    maybe (unexpected $ JSON.String txt) return . readMaybe $ unpack txt

instance ToJSON IPv4
instance ToJSON IPv6
instance ToJSON IP
instance ToJSON a => ToJSON (AddrRange a)

instance ToJSON IPRange where
    toJSON = JSON.String . pack . show


instance (Hashable a) => Hashable (AddrRange a) where
    hashWithSalt salt range =
        hashWithSalt (hashWithSalt salt $ addr range) $ mlen range
    hash = hashWithSalt $ fromIntegral fnvOffsetBasis32

instance Hashable IPv6 where
    hashWithSalt salt ip6 =
        let (d,c,b,a) = toHostAddress6 ip6
            calcHash  = hashFn d >> hashFn c >> hashFn b >> hashFn a
        in fromIntegral $ execState calcHash $ fromIntegral salt

instance Binary Family where
    put = put . (fromIntegral :: CInt -> Int) . packFamily
    get = get <&> unpackFamily . (fromIntegral :: Int -> CInt)

-- | make SockAddr type an instance of Read class, so that strings produced with
-- | the inverse "show" operation can be deserialized. Note the use of unsafePerformIO
-- | here.. this is a result of getAddrInfo returning a result wrapped in the IO Monad
-- | but since we call that function with the AI_NUMERICHOST option, it will *not*
-- | perform a DNS lookup and in effect just a parsing of the IP string. This means
-- | that our use is referential transparent. The compiler has of course no way
-- | of knowing that, but we do. Ergo.. this is a legitimate case where breaking out
-- | of IO is ok since we are claiming to know what we are doing :-)
instance Read SockAddr where
    -- | parse a V6 socket address
    readsPrec _ ('[':str)  =
        let (address', rest) = span (/= ']') $ dropWhile isSpace str -- extract the IPv6 portion
            rest'           = drop 2 rest       -- skip the ]:
            (port', remains) = span isDigit rest'
        in readSockAddr AF_INET6 address' port' remains
    -- | parse a V4 socket address
    readsPrec _ str =
        let (address', rest) = span (/= ':') $ dropWhile isSpace str -- extract IPv4 address
            (port', remains) = span isDigit $ drop 1 rest -- skip the : and expect the numeric port
        in readSockAddr AF_INET address' port' remains

instance Binary SockAddr where
    put a = put $ show a
    get   = read <$> get

instance FromJSON PortNumber where
  parseJSON = withScientific "PortNumber" parse'
    where parse' v
            | Just value <- toBoundedInteger v =
                if value >= 0 && value <= 65535
                  then return $ fromIntegral (value :: Int)
                  else fail $ "Port number out of range: " <> show value
            | otherwise =
                fail $ "Not a port number: " <> show v

instance ToJSON PortNumber where
  toJSON = JSON.Number . fromIntegral

data PortRange = PortRange {
                    fromPort :: PortNumber,
                    toPort   :: PortNumber
                 }

instance Show PortRange where
  showsPrec p (PortRange start end)
    | start == end = showsPrec p start
    | otherwise    = showsPrec p start . (':':) . showsPrec p end

rangeWith :: Read a => (Char -> Bool) -> String -> [((a, a), String)]
rangeWith p str = fromMaybe mempty $ do
  let (fromStr, rest) = span p str
  start <- readMaybe fromStr
  case rest of
    [] ->
      return [((start, start), "")]
    (_:toStr) -> do
      end <- readMaybe toStr
      return [((start, end), "")]

instance Read PortRange where
  readsPrec _ =
    over (traverse . _1) (uncurry PortRange) . rangeWith (\x -> x /= ':' && x /= '-')

instance FromJSON PortRange where
  parseJSON = withText "PortRange" $ \txt ->
    maybe (unexpected $ JSON.String txt) return . readMaybe $ unpack txt

instance ToJSON PortRange where
  toJSON = JSON.String . pack . show

v6NetworkAddress :: A.Parser (ByteString, Family)
v6NetworkAddress =
  wrapped v6Address

v4NetworkAddress :: A.Parser (ByteString, Family)
v4NetworkAddress = do
  addr <- v4Address
  return (addr, AF_INET)

portNumber :: A.Parser ByteString
portNumber =
  A.takeWhile1 A.isDigit

validHost :: A.Parser (ByteString, Family)
validHost = wrapped host <|> do
              addr <- host
              return (addr, AF_INET)
    where host = A.takeWhile1 $ A.inClass ".a-zA-Z0-9-"

wrapped :: A.Parser ByteString -> A.Parser (ByteString, Family)
wrapped parser = do
  void $ A.char8 '['
  val <- parser
  void $ A.char8 ']'
  return (val, AF_INET6)

v4Address :: A.Parser ByteString
v4Address = do
  (addr, _) <- A.match $ do
                a <- A.takeWhile1 A.isDigit
                guard $ isValidOctet a
                void $ A.char8 '.'
                b <- A.takeWhile1 A.isDigit
                guard $ isValidOctet b
                void $ A.char8 '.'
                c <- A.takeWhile1 A.isDigit
                guard $ isValidOctet c
                void $ A.char8 '.'
                d <- A.takeWhile1 A.isDigit
                guard $ isValidOctet d
  return addr
      where isValidOctet =
                maybe False (\(x::Int) -> 0 <= x && x < 256) . readMaybe . B.unpack

v6Address :: A.Parser ByteString
v6Address = do
  (addr, _) <- A.match $
                type1 <|>
                type2 <|>
                type3
  return addr
      where
            v6Word :: A.Parser ByteString
            v6Word = do
              x <- A.many1 $ A.satisfy isHexDigit
              guard $ length x <= 4
              return . B.pack $ x
            v4section = A.string "ffff:" >> v4Address
            v6section = mconcat <$> A.sepBy v6Word (A.char8 ':') 
            type1     =
              A.string "::" >> (v4section <|> v6section) >> return ()
            type2     = do
              s <- A.sepBy1 v6section (A.string "::")
              guard $ length s <= 2
              return ()
            type3     =
              v6section >> A.string "::" >> return ()

hashFn :: Word32 -> State Word32 ()
hashFn x = modify $ flip fnv1aHash32Word32 x

addressFamily :: SockAddr -> Family
addressFamily SockAddrInet { }  = AF_INET
addressFamily SockAddrInet6 { } = AF_INET6
addressFamily SockAddrUnix { }  = AF_UNIX

addressFromSockAddr :: SockAddr -> IP
addressFromSockAddr (SockAddrInet _ v4Host)              = IPv4 $ fromHostAddress v4Host
addressFromSockAddr (SockAddrInet6 _ _ (0,0,0xFFFF,x) _) = IPv4 $ toIPv4w x
addressFromSockAddr (SockAddrInet6 _ _ v6Host _)         = IPv6 $ fromHostAddress6 v6Host
addressFromSockAddr address' = throw $ BadSockAddr (show address')

portFromSockAddr :: SockAddr -> PortNumber
portFromSockAddr (SockAddrInet port' _)      = port'
portFromSockAddr (SockAddrInet6 port' _ _ _) = port'
portFromSockAddr sAddr                      = throw $ BadSockAddr $ show sAddr

sockAddrFrom ::  IP -> PortNumber -> SockAddr
sockAddrFrom (IPv4 ip) port' = SockAddrInet port' (toHostAddress ip)
sockAddrFrom (IPv6 ip) port' = SockAddrInet6 port' 0 (toHostAddress6 ip) 0

addressBits :: IP -> Int
addressBits (IPv4 _) = 32
addressBits (IPv6 _) = 128

family :: IP -> Family
family (IPv4 _) = AF_INET
family (IPv6 _) = AF_INET6

readSockAddr :: Family -> String -> String -> String -> [(SockAddr, String)]
readSockAddr fam address' port' remains =
    let hint   = Just (defaultHints {addrFlags  = [AI_NUMERICHOST],
                                     addrFamily = fam})
        infos  = getAddrInfo hint (Just address') (Just port')
    in unsafePerformIO $ (take 1 . map ((,remains) . addrAddress) <$> infos) `catch`
         \(_::SomeException) -> return []

peekIP :: Word8 -> Ptr Word32 -> IO IP
peekIP 1 ptr =
    IPv4 . fromHostAddress <$> peek ptr
peekIP 4 ptr = do
    w1 <- peek ptr
    w2 <- peek $ plusPtr ptr 0x4
    w3 <- peek $ plusPtr ptr 0x8
    w4 <- peek $ plusPtr ptr 0xC
    return . IPv6 . fromHostAddress6 $ (w1, w2, w3, w4)
peekIP n _ =
    throw $ InvalidIPAddressLength n

pokeIP :: Ptr Word32 -> IP -> IO ()
pokeIP ptr (IPv4 ip4) =
    poke ptr $ toHostAddress ip4
pokeIP ptr (IPv6 ip6) =
    let (w1, w2, w3, w4) = toHostAddress6 ip6
    in do
      poke ptr               w1
      poke (plusPtr ptr 0x4) w2
      poke (plusPtr ptr 0x8) w3
      poke (plusPtr ptr 0xC) w4

addressLengthFor :: Family -> Word8
addressLengthFor AF_INET  = 1
addressLengthFor AF_INET6 = 4
addressLengthFor af       = throw $ UnsupportedAddressFamily af

isV4 :: IP -> Bool
isV4 (IPv4 _) = True
isV4 _        = False

isV6 :: IP -> Bool
isV6 (IPv6 _) = True
isV6 _        = False

toIPv6 :: IP -> IPv6
toIPv6 (IPv6 ipv6) = ipv6
toIPv6 (IPv4 ipv4) = ipv4ToIPv6 ipv4

isIPReserved :: IP -> Bool
isIPReserved (IPv4 ip4) = isIP4Reserved ip4
isIPReserved (IPv6 ip6) = isIP6Reserved ip6

isIP4Private :: IPv4 -> Bool
isIP4Private ip4 =
     0xFF000000 .&. ip4w == fromOctets [0, 0, 0, 0]
  || 0xFFF00000 .&. ip4w == fromOctets [172, 16, 0, 0]
  || 0xFFFF0000 .&. ip4w == fromOctets [192, 168, 0, 0]
  where ip4w = fromIPv4w ip4

isIP4Reserved :: IPv4 -> Bool
isIP4Reserved ip4 =
     isIP4Private ip4
  || 0xFF000000 .&. ip4w == fromOctets [0, 0, 0, 0]
  || 0xFF000000 .&. ip4w == fromOctets [10, 0, 0, 0]
  || 0xFFC00000 .&. ip4w == fromOctets [100, 64, 0, 0]
  || 0xFFFF0000 .&. ip4w == fromOctets [169, 254, 0, 0]
  || 0xFFFFFF00 .&. ip4w == fromOctets [192, 0, 0, 0]
  || 0xFFFFFF00 .&. ip4w == fromOctets [192, 88, 99, 0]
  || 0xFFFE0000 .&. ip4w == fromOctets [198, 18, 0, 0]
  || 0xFFFFFF00 .&. ip4w == fromOctets [198, 51, 100, 0]
  || 0xFFFFFF00 .&. ip4w == fromOctets [203, 0, 113, 0]
  || 0xF0000000 .&. ip4w == fromOctets [224, 0, 0, 0]
  || 0xF0000000 .&. ip4w == fromOctets [240, 0, 0, 0]
  || 0xFFFFFFFF .&. ip4w == fromOctets [255, 255, 255, 255]
  where ip4w = fromIPv4w ip4

isIP6Private :: IPv6 -> Bool
isIP6Private = flip isMatchedTo
  $ flip makeAddrRange 7
  $ toIPv6b [252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

isIP6Reserved :: IPv6 -> Bool
isIP6Reserved ip6 =
     isIP6Private ip6
  || isIP6LinkLocal ip6
  || isIP6Doc ip6
  || isIP6Loopback ip6
  where
    isIP6LinkLocal = isMatched 10 $ toIPv6b [254,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    isIP6Doc       = isMatched 32 $ toIPv6b [32,1,219,128,0,0,0,0,0,0,0,0,0,0,0,0]
    isIP6Loopback  = isMatched 128 $ toIPv6b [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]
    isMatched len  = flip isMatchedTo . flip makeAddrRange len

fromOctets :: [Int] -> Word32
fromOctets = fromIPv4w . toIPv4
