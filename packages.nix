#
{dev, haskellLib, ...}: self: super: let

in {
  mkDerivation = args : super.mkDerivation (args // {
    doCheck   = false;
    doHaddock = false;
    enableLibraryProfiling = dev;
  });

  ghc = super.ghc;

  ncore = self.callCabal2nix "ncore" (
               builtins.filterSource (path: type: type != "directory" ||
                                      baseNameOf path != ".git") ./. ) { };

}
